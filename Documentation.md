# Analytics

## Introduction

The FG Analytics plugin allow you to track events in your application. It will help you to understand better how your game behave once its live, and give you the possibility to track any kind of information through :

- Progression events
- Ad events
- Design events

In FunGames SDK, most usefull events are already handled in the SDK so that we can easily measure data like :

- The number of new users
- Session time
- Number of sessions
- Playtime
- Retention
- Ad impressions
- etc.

## Scripting API

Once an Analytics sub module is added to your project you will be able to send custom events using the FGAnalytics static methods :

```csharp
NewProgressionEvent(LevelStatus status, string level, string subLevel = "", int score = -1)
NewDesignEvent(string eventId, float eventValue = 0)
NewDesignEvent(string eventId, Dictionary<string, object> customFields, float eventValue = 0)
NewAdEvent(AdAction adAction, AdType adType, string adSdkName, string adPlacement)
```

This the exemple about how to use them :

```csharp
FGAnalytics.NewDesignEvent("EventName", 1);
FGAnalytics.NewProgressionEvent(LevelStatus.Start,"level1");
FGAnalytics.NewProgressionEvent(LevelStatus.Complete,"level1","end2",24);
FGAnalytics.NewProgressionEvent(LevelStatus.Fail, "level1", "end2");
FGAnalytics.NewAdEvent(AdAction.Show, AdType.Interstitial, "network", "placement");
```
You can also subscribe to these callbacks if you want to attach custom actions at initialization or when an event is sent:

```csharp
FGAnalytics.Callbacks.Initialization;
FGAnalytics.Callbacks.OnInitialized;
FGAnalytics.Callbacks.OnSendProgressionEvent;
FGAnalytics.Callbacks.OnSendDesignEventSimple;
FGAnalytics.Callbacks.OnSendDesignEventDictio;
FGAnalytics.Callbacks.OnSendAdEvent;
```